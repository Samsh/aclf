"use strict";

// ES5 and jQuery like it's 2013. Yeah, Baby, yeah !

// Webpack import stuff

require("./sass/styles.scss");
require("jquery.scrollto");
require("jquery.easing");
var $ = require("jquery");

// Main stuff

console.log("ok");

var clickEvent = "click";

$(document).ready(function() {
  var menuContent = $("header nav").clone();
  $("#menu").append(menuContent);

  $("[data-target]").bind(clickEvent, clickScroll);
  $("[data-video]").bind(clickEvent, clickVideoLink);
  $("#videopopin .close").bind(clickEvent, closeVideoPopin);
  $("#menubtn").bind(clickEvent, toggleMenu);
});

function toggleMenu() {
  //$("body").toggleClass("sticky");
  $("#menu").toggleClass("shown");
}

function clickVideoLink(e) {
  e.preventDefault();
  openVideoPopin($(e.currentTarget).attr("data-video"));
}

function openVideoPopin(src) {
  $("#videopopin").addClass("shown");
  var videoTag = '<video src="' + src + '" controls></video>';
  $("#videopopin .box").html(videoTag);
  $(window).bind("resize", onResize);
  $("#videopopin video").one("loadedmetadata", function(e) {
    $(window).resize();
    $(e.target)
      .get(0)
      .play();
  });
}

function onResize() {
  var winHeight = $(window).height();
  var videoHeight = $("#videopopin video").height();
  var maxHeight = winHeight - 40;
  if (videoHeight > maxHeight) {
    videoHeight = maxHeight;
    $("#videopopin video").height(maxHeight);
  }
  $("#videopopin .box").css("margin-top", -Math.round(videoHeight / 2));
}

function closeVideoPopin(e) {
  e.preventDefault();
  $("#videopopin video")
    .get(0)
    .pause();
  $(window).unbind("resize");
  $("#videopopin .box").html("");
  $("#videopopin").removeClass("shown");
}

function clickScroll(e) {
  e.preventDefault();
  if ($("#menu").hasClass("shown")) {
    toggleMenu();
  }
  scrollToSection(
    $(e.currentTarget).attr("data-target"),
    $(e.currentTarget).attr("data-duration")
  );
}

function scrollToSection(id, duration) {
  duration = duration ? duration : 600;
  var toY = $("#" + id).offset().top - $("header").outerHeight();
  $.scrollTo(toY, { duration: 600, easing: "easeOutQuad" });
}
